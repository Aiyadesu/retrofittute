package aiya.retrofittute;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;

public class PosterActivity extends AppCompatActivity {

    // region Activity Fields

    private static final String MSG_POSTER = "aiya.retrofit.message.POSTER";

    private static final String BASE_URL = "http://image.tmdb.org/t/p/original/";

    // endregion

    // region Constructor
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poster);

        new DownloadPosterTask().execute();
    }
    // endregion

    // region DownloadPosterTask
    private class DownloadPosterTask extends AsyncTask<URL, Integer, Void> {

        Bitmap bmp;

        /*
        TODO:
        How do I change the return type to 'void'?
        I tried changing the 'Result' type in the extension declaration AsyncTask<URL, Integer, void>
        but it says 'void' is an illegal type
        'Void' is another option but still requires me to return a type 'Void' instead of no return type
        as the case for 'void'
         */
        protected Void doInBackground(URL... urls) {
            /*
        In try-catch block because it throws a 'MalformedURLException' checked exception.
        https://stackoverflow.com/questions/24416930/java-malformed-url-exception
         */
            try {
                URL url = new URL(BASE_URL + getIntent().getStringExtra(MSG_POSTER));
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            return (Void) null;
        }

        protected void onPostExecute(Void result) {
            ImageView poster = (ImageView) findViewById(R.id.posterView);

            poster.setImageBitmap(bmp);
        }
    }
    // endregion
}
